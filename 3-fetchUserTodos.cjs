fetch('https://jsonplaceholder.typicode.com/users')
.then((response) => response.json()
)
.then((userData)=>{
    let todosData=fetch('https://jsonplaceholder.typicode.com/todos')
    return Promise.all([userData,todosData]);
})
.then((array)=> Promise.all([array[0],array[1].json()]))
.then((result)=>{
    console.log(result[0]);
    console.log(result[1]);
})
.catch((err)=> console.log(err))
