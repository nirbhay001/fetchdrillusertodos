fetch('https://jsonplaceholder.typicode.com/users')
.then((responseUser) => responseUser.json())
.then((userData)=>{
    let userDetail=userData.map((item)=>{
        return fetch(`https://jsonplaceholder.typicode.com/users?id=${item.id}`);
    })
    return Promise.all(userDetail)
})
.then((detailConverted)=>{
    let resultJson=detailConverted.map((item)=>{
        return item.json();
    })
    return Promise.all(resultJson);
})
.then((convertedData)=> console.log(convertedData))
.catch((error) => console.log(error))
