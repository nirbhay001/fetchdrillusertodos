
fetch(`https://jsonplaceholder.typicode.com/todos?id=${1}`)
.then((responseTodo) => responseTodo.json())
.then((responseData) => fetch(`https://jsonplaceholder.typicode.com/users?id=${responseData[0].userId}`))
.then((responseUser) => responseUser.json())
.then((responseUserData) => console.log(responseUserData))
.catch((error) => console.log(error));
